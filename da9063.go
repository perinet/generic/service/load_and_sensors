package main

import (
	"time"
)

var (
	da9063_voltage_rails []monitor_rail
	da9063_temp_values   []monitor_rail
)

func define_voltages_da9063() {
	da9063_voltage_rails = []monitor_rail{
		{
			Name:        "1V1",
			Entries:     0.0,
			adc_mux_reg: ADC_MUX_REG1,
			mon_ax_idx:  MON_A8_IDX_BUCKCORE1,
		},
		{
			Name:        "0V6",
			Entries:     0.0,
			adc_mux_reg: ADC_MUX_REG1,
			mon_ax_idx:  MON_A8_IDX_BUCKPRO,
		},
		{
			Name:        "2V5",
			Entries:     0.0,
			adc_mux_reg: ADC_MUX_REG1,
			mon_ax_idx:  MON_A8_IDX_LDO11,
		},
		{
			Name:        "1V8",
			Entries:     0.0,
			adc_mux_reg: ADC_MUX_REG2,
			mon_ax_idx:  MON_A9_IDX_BUCKIO,
		},
		{
			Name:        "1V2",
			Entries:     0.0,
			adc_mux_reg: ADC_MUX_REG2,
			mon_ax_idx:  MON_A9_IDX_BUCKMEM,
		},
		{
			Name:        "3V3",
			Entries:     0.0,
			adc_mux_reg: ADC_MUX_REG2,
			mon_ax_idx:  MON_A9_IDX_BUCKPERI,
		},
		{
			Name:        "1V8_Quiet",
			Entries:     0.0,
			adc_mux_reg: ADC_MUX_REG2,
			mon_ax_idx:  MON_A9_IDX_LDO2,
		},
		{
			Name:        "3V3_Quiet",
			Entries:     0.0,
			adc_mux_reg: ADC_MUX_REG2,
			mon_ax_idx:  MON_A9_IDX_LDO5,
		},
		{
			Name:        "5V0",
			Entries:     0.0,
			adc_mux_reg: ADC_MUX_VSYS,
			mon_ax_idx:  MON_AX_NONE,
		},
	}
}

func define_temp_da9063() {
	da9063_temp_values = []monitor_rail{
		{
			Name:        "PMIC_Temperature",
			Entries:     0.0,
			adc_mux_reg: ADC_MUX_TEMP,
			mon_ax_idx:  MON_AX_NONE,
		},
		{
			Name:    "LM75",
			Entries: 0.0,
		},
	}
}

const da9063_i2c_bus int = 0

const da9063_addr uint8 = 0xb4 >> 1
const adc_resolution float32 = 0.004883

const (
	Control_E uint8 = 0x12
	EVENT_A   uint8 = 0x06
	ADC_MAN   uint8 = 0x34
	ADC_RES_H uint8 = 0x38
	ADC_RES_L uint8 = 0x37
	PAGE_CON  uint8 = 0x00
	T_OFFSET  uint8 = 0x04
	MON_REG_5 uint8 = (0x11E & 0xff) // The first 4 bit can be ignored due to page config
)

const (
	MON_AX_NONE         uint8 = 0b0
	MON_A9_IDX_BUCKIO   uint8 = 0b1 << 4
	MON_A9_IDX_BUCKMEM  uint8 = 0b10 << 4
	MON_A9_IDX_BUCKPERI uint8 = 0b11 << 4
	MON_A9_IDX_LDO2     uint8 = 0b101 << 4
	MON_A9_IDX_LDO5     uint8 = 0b110 << 4

	MON_A8_IDX_BUCKCORE1 uint8 = 0b1
	MON_A8_IDX_BUCKPRO   uint8 = 0b11
	MON_A8_IDX_LDO11     uint8 = 0b110
)

const (
	ADC_start    uint8 = 0b1 << 4
	ADC_MUX_REG1 uint8 = 0b1000
	ADC_MUX_REG2 uint8 = 0b1001
	ADC_MUX_TEMP uint8 = 0b0100
	ADC_MUX_VSYS uint8 = 0b0000
)

func clearBit(n byte, pos uint) byte {
	mask := ^(0x1 << pos)
	n &= byte(mask)
	return n
}

func setBit(n byte, pos uint) byte {
	n |= (0x1 << byte(pos))
	return n
}

func get_adc_result(dev *I2C) int {
	value, _ := dev.ReadRegU8(EVENT_A)
	//logger.Printf("EVENT_A Reg: 0x%0X", value)
	dev.WriteRegU8(EVENT_A, value)
	value, _ = dev.ReadRegU8(EVENT_A)
	//logger.Printf("EVENT_A Reg: 0x%0X", value)

	value, _ = dev.ReadRegU8(ADC_MAN)
	value |= ADC_start
	dev.WriteRegU8(ADC_MAN, value)
	// _, _ = dev.ReadRegU8(EVENT_A)
	// time.Sleep(time.Second)
	// _, _ = dev.ReadRegU8(EVENT_A)

	i := 0
	for {
		value, _ = dev.ReadRegU8(EVENT_A)
		//logger.Printf("EVENT_A Reg: 0x%0X", value)
		if ((value & 0x8) != 0x0) || !(i < 10) {
			break
		}
		time.Sleep(time.Millisecond)
		//logger.Printf("loop counter: %d", i)
		i++
	}

	adc_h, _ := dev.ReadRegU8(ADC_RES_H)
	adc_l, _ := dev.ReadRegU8(ADC_RES_L)

	adc_res := int(adc_h)
	adc_res = adc_res << 2
	adc_res |= int(adc_l >> 6)
	// logger.Printf("adc_result: %d", adc_res)
	// result := float32(adc_res) * adc_resolution
	// logger.Printf("adc_result: %f", result)
	//return result
	return adc_res
}

func da9063_get_temp(adc_mux_reg uint8, mon_ax_idx uint8) float32 {
	//logger.Println("Start voltage")
	dev, err := I2C_dev(da9063_addr, da9063_i2c_bus)
	if err != nil {
		logger.Panic(err)
	}

	//Get control_e reg
	value, _ := dev.ReadRegU8(Control_E)
	//clear v_lock bit
	value = clearBit(value, 7)
	//write back
	dev.WriteRegU8(Control_E, value)

	//Go to PAGE 2
	dev.WriteRegU8(PAGE_CON, 0x42)

	dev.WriteRegU8(MON_REG_5, mon_ax_idx)

	value, _ = dev.ReadRegU8(T_OFFSET)
	t_offset := float32(int8(value))
	//Jump back to Page 0
	dev.WriteRegU8(PAGE_CON, 0x40)

	value, _ = dev.ReadRegU8(ADC_MAN)
	//Clear lower 4 bits of mux
	value &= 0xf0
	dev.WriteRegU8(ADC_MAN, (value | adc_mux_reg))
	//Set manual conversion
	adc_res := get_adc_result(dev)
	result := -0.398*(float32(adc_res)-t_offset) + 330
	//logger.Printf("Start voltage %f", result)

	//Get control_e reg
	value, _ = dev.ReadRegU8(Control_E)
	//clear set v_lock bit
	value = setBit(value, 7)
	//write back
	dev.WriteRegU8(Control_E, value)

	dev.Close()

	return result
}

func da9063_get_voltage(adc_mux_reg uint8, mon_ax_idx uint8) float32 {
	//logger.Println("Start voltage")
	dev, err := I2C_dev(da9063_addr, da9063_i2c_bus)
	if err != nil {
		logger.Panic(err)
	}

	//Get control_e reg
	value, _ := dev.ReadRegU8(Control_E)
	//clear v_lock bit
	value = clearBit(value, 7)
	//write back
	dev.WriteRegU8(Control_E, value)

	//Go to PAGE 2
	dev.WriteRegU8(PAGE_CON, 0x42)

	dev.WriteRegU8(MON_REG_5, mon_ax_idx)
	//Jump back to Page 0
	dev.WriteRegU8(PAGE_CON, 0x40)

	value, _ = dev.ReadRegU8(ADC_MAN)
	//Clear lower 4 bits of mux
	value &= 0xf0
	dev.WriteRegU8(ADC_MAN, (value | adc_mux_reg))
	//Set manual conversion
	adc_res := get_adc_result(dev)
	result := float32(adc_res) * adc_resolution
	//logger.Printf("Start voltage %f", result)

	//Get control_e reg
	value, _ = dev.ReadRegU8(Control_E)
	//clear set v_lock bit
	value = setBit(value, 7)
	//write back
	dev.WriteRegU8(Control_E, value)

	dev.Close()

	return result
}

// // func http_endpoint_voltage(w http.ResponseWriter, r *http.Request) {
// // 	w.Header().Set("Content-type", "application/json; charset=utf-8;")

// // 	if r.Method == http.MethodGet {
// // 		device_mutex.Lock()
// // 		var voltages_list voltages
// // 		voltages_list.Voltage_rails = voltage_rails
// // 		data, err := json.Marshal(voltages_list)
// // 		device_mutex.Unlock()
// // 		if err != nil {
// // 			w.WriteHeader(http.StatusInternalServerError)
// // 			w.Write(json.RawMessage(`{"error": "cannot marshal object to json"}`))
// // 		} else {
// // 			w.WriteHeader(http.StatusOK)
// // 			w.Write(data)
// // 		}
// // 	} else {
// // 		w.WriteHeader(http.StatusMethodNotAllowed)
// // 	}
// // }
