package main

import (
	"os/exec"
	"strconv"
	"strings"
	"sync"
	"time"
)

type sensors struct {
	Name    string   `json:"name"`
	Adapter string   `json:"adapter"`
	Entries []string `json:"entries"`
}

// type sensors_data struct {
// 	Sensors []sensors `json:"sensors"`
// }

var (
	sensor_list_mutex sync.Mutex
	sensor_list       []sensors
)

func get_sensors() []sensors {
	out, err := exec.Command("sensors").Output()
	if err != nil {
		logger.Print(err)
		return nil
	}
	res := strings.Split(string(out), "\n")

	var temp_list []sensors
	var sensor *sensors
	var sensor_line_index int
	for _, line := range res {
		if sensor_line_index == 0 {
			sensor = new(sensors)
			sensor.Name = line
		}
		if sensor_line_index == 1 {
			ad := strings.Split(line, ":")
			if len(ad) > 1 {
				sensor.Adapter = strings.TrimSpace(ad[1])
			}
		}
		if sensor_line_index > 1 {
			if len(line) > 0 {
				sensor.Entries = append(sensor.Entries, line)
			} else {
				temp_list = append(temp_list, *sensor)
				sensor_line_index = -1
			}
		}
		sensor_line_index += 1
	}
	return temp_list
}

func lm75_get_temp() float32 {
	var result float32
	for idx, _ := range sensor_list {
		name := sensor_list[idx].Name
		if strings.Contains(name, "lm75") {
			value := strings.ReplaceAll(sensor_list[idx].Entries[0], " ", "")
			value = strings.ReplaceAll(value, "°", "")
			value = strings.Split(value, ":")[1]
			value = strings.Split(value, "C")[0]
			temp, _ := strconv.ParseFloat(value, 32)
			result = float32(temp)
			break
		}
	}
	return result
}

func obtain_sensors() {
	for {
		sensor_list_mutex.Lock()
		sensor_list = get_sensors()
		sensor_list_mutex.Unlock()
		time.Sleep(time.Second)
	}
}

// func http_endpoint_sensors(w http.ResponseWriter, r *http.Request) {
// 	w.Header().Set("Content-type", "application/json; charset=utf-8;")
// 	if r.Method == http.MethodGet {
// 		sensor_list_mutex.Lock()
// 		var sensor sensors_data
// 		sensor.Sensors = sensor_list
// 		data, err := json.Marshal(sensor)
// 		sensor_list_mutex.Unlock()
// 		if err != nil {
// 			w.WriteHeader(http.StatusInternalServerError)
// 			w.Write(json.RawMessage(`{"error": "cannot marshal object to json"}`))
// 		} else {
// 			w.WriteHeader(http.StatusOK)
// 			w.Write(data)
// 		}
// 	} else {
// 		w.WriteHeader(http.StatusMethodNotAllowed)
// 	}
// }
