package main

import (
	"fmt"
	"os"
	"syscall"
)

// Get I2C_SLAVE constant value from
// Linux OS I2C declaration file.
const (
	I2C_SLAVE       = 0x0703
	I2C_SLAVE_FORCE = 0x0706
)

type I2C struct {
	addr uint8
	bus  int
	fd   *os.File
}

func I2C_dev(addr uint8, bus int) (*I2C, error) {
	f, err := os.OpenFile(fmt.Sprintf("/dev/i2c-%d", bus), os.O_RDWR, 0600)
	if err != nil {
		return nil, err
	}
	if err := ioctl(f.Fd(), I2C_SLAVE, uintptr(addr)); err != nil {
		return nil, err
	}
	dev := &I2C{fd: f, bus: bus, addr: addr}
	return dev, nil
}

func I2C_dev_force(addr uint8, bus int) (*I2C, error) {
	f, err := os.OpenFile(fmt.Sprintf("/dev/i2c-%d", bus), os.O_RDWR, 0600)
	if err != nil {
		return nil, err
	}
	if err := ioctl(f.Fd(), I2C_SLAVE_FORCE, uintptr(addr)); err != nil {
		return nil, err
	}
	dev := &I2C{fd: f, bus: bus, addr: addr}
	return dev, nil
}

func (dev *I2C) GetBus() int {
	return dev.bus
}

func (dev *I2C) GetAddr() uint8 {
	return dev.addr
}

func (dev *I2C) write(buf []byte) (int, error) {
	return dev.fd.Write(buf)
}

func (dev *I2C) Close() error {
	return dev.fd.Close()
}
func (dev *I2C) WriteBytes(buf []byte) (int, error) {
	//logger.Printf("Write %d hex bytes: [%+v]\n", len(buf), hex.EncodeToString(buf))
	return dev.write(buf)
}

func (dev *I2C) read(buf []byte) (int, error) {
	return dev.fd.Read(buf)
}

func (dev *I2C) ReadBytes(buf []byte) (int, error) {
	n, err := dev.read(buf)
	if err != nil {
		return n, err
	}
	//logger.Printf("Read %d hex bytes: [%+v]\n", len(buf), hex.EncodeToString(buf))
	return n, nil
}

func (dev *I2C) ReadRegU8(reg byte) (byte, error) {
	_, err := dev.WriteBytes([]byte{reg})
	if err != nil {
		return 0, err
	}
	buf := make([]byte, 1)
	_, err = dev.ReadBytes(buf)
	if err != nil {
		return 0, err
	}
	//logger.Printf("Read U8 0x%0X from reg 0x%0X\n", buf[0], reg)
	return buf[0], nil
}

func (dev *I2C) WriteRegU8(reg byte, value byte) error {
	buf := []byte{reg, value}
	_, err := dev.WriteBytes(buf)
	if err != nil {
		return err
	}
	//logger.Printf("Write U8 0x%0X to reg 0x%0X", value, reg)
	return nil
}

func ioctl(fd, cmd, arg uintptr) error {
	_, _, err := syscall.Syscall6(syscall.SYS_IOCTL, fd, cmd, arg, 0, 0, 0)
	if err != 0 {
		return err
	}
	return nil
}
