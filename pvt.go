package main

import (
	"encoding/json"
	"net/http"
	"sync"
	"time"
)

type monitor_rail struct {
	Name        string  `json:"name"`
	Entries     float32 `json:"entries"`
	adc_mux_reg uint8
	mon_ax_idx  uint8
}

type pvt struct {
	Voltage_rails     []monitor_rail `json:"voltage_rails"`
	Temperature_rails []monitor_rail `json:"temperature"`
	Process           []cpu          `json:"process"`
}

type pvt_data struct {
	Incarnation_counter int `json:"incarnation"`
	Sequence            int `json:"sequence"`
	Data                pvt `json:"data"`
}

var (
	device_mutex        sync.Mutex
	voltage_rails       []monitor_rail
	temp_values         []monitor_rail
	sequence            int
	incarnation_counter int
)

func obtain_pvt(numProcessors int) {
	sequence = 0
	incarnation_counter = 0
	for {
		device_mutex.Lock()
		if numProcessors == 2 {
			voltage_rails = da9063_voltage_rails
			temp_values = da9063_temp_values
			for idx, _ := range voltage_rails {
				voltage_rails[idx].Entries = da9063_get_voltage(voltage_rails[idx].adc_mux_reg, voltage_rails[idx].mon_ax_idx)
			}
			temp_values[0].Entries = da9063_get_temp(temp_values[0].adc_mux_reg, temp_values[0].mon_ax_idx)
		} else {
			voltage_rails = tps65217_voltage_rails
			temp_values = tps65217_temp_values
			for idx, _ := range voltage_rails {
				voltage_rails[idx].Entries = tps65217_get_voltage(voltage_rails[idx].adc_mux_reg, voltage_rails[idx].mon_ax_idx)
			}
		}
		temp_values[1].Entries = lm75_get_temp()
		sequence++
		device_mutex.Unlock()
		time.Sleep(time.Second)
	}
}

func http_endpoint_pvt(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-type", "application/json; charset=utf-8;")

	if r.Method == http.MethodGet {
		device_mutex.Lock()
		var pvt_request pvt_data
		pvt_request.Data.Temperature_rails = temp_values
		pvt_request.Data.Voltage_rails = voltage_rails
		pvt_request.Data.Process = cpus
		pvt_request.Sequence = sequence
		pvt_request.Incarnation_counter = incarnation_counter
		// temp_list.Voltage_rails = voltage_rails
		// temp_list.Temperature_rails = temp_values
		// temp_list.Process = cpus

		data, err := json.Marshal(pvt_request)
		device_mutex.Unlock()
		if err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			w.Write(json.RawMessage(`{"error": "cannot marshal object to json"}`))
		} else {
			w.WriteHeader(http.StatusOK)
			w.Write(data)
		}
	} else {
		w.WriteHeader(http.StatusMethodNotAllowed)
	}
}
