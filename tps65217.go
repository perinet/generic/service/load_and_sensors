package main

var (
	tps65217_voltage_rails []monitor_rail
	tps65217_temp_values   []monitor_rail
)

func define_voltages_tps65217() {
	tps65217_voltage_rails = []monitor_rail{
		{
			Name:        "1V5",
			Entries:     0.0,
			adc_mux_reg: tps65217_PGOOD,
			mon_ax_idx:  DC1_PG,
		},
		{
			Name:        "1V1_MPU",
			Entries:     0.0,
			adc_mux_reg: tps65217_PGOOD,
			mon_ax_idx:  DC2_PG,
		},
		{
			Name:        "0V9_CORE",
			Entries:     0.0,
			adc_mux_reg: tps65217_PGOOD,
			mon_ax_idx:  DC3_PG,
		},
		{
			Name:        "1V0",
			Entries:     0.0,
			adc_mux_reg: tps65217_PGOOD,
			mon_ax_idx:  LDO1_PG,
		},
		{
			Name:        "0V9",
			Entries:     0.0,
			adc_mux_reg: tps65217_PGOOD,
			mon_ax_idx:  LDO2_PG,
		},
		{
			Name:        "1V8",
			Entries:     0.0,
			adc_mux_reg: tps65217_PGOOD,
			mon_ax_idx:  LDO3_PG,
		},
		{
			Name:        "3V3",
			Entries:     0.0,
			adc_mux_reg: tps65217_PGOOD,
			mon_ax_idx:  LDO4_PG,
		},
	}
}

func define_temp_tps65217() {
	tps65217_temp_values = []monitor_rail{
		{
			Name:        "PMIC_Temperature",
			Entries:     0.0,
			adc_mux_reg: 0,
			mon_ax_idx:  0,
		},
		{
			Name:    "LM75",
			Entries: 0.0,
		},
	}
}

const tps65217_addr uint8 = 0x24
const tps65217_i2c_bus int = 0

const (
	tps65217_MUXCTRL uint8 = 0x09
	tps65217_PGOOD   uint8 = 0x0c
)

const (
	LDO3_PG uint8 = 0x1 << 6
	LDO4_PG uint8 = 0x1 << 5
	DC1_PG  uint8 = 0x1 << 4
	DC2_PG  uint8 = 0x1 << 3
	DC3_PG  uint8 = 0x1 << 2
	LDO1_PG uint8 = 0x1 << 1
	LDO2_PG uint8 = 0x1
)

func tps65217_get_voltage(adc_mux_reg uint8, mon_ax_idx uint8) float32 {
	//logger.Println("Start voltage")
	var result float32
	dev, err := I2C_dev_force(tps65217_addr, tps65217_i2c_bus)
	if err != nil {
		logger.Panic(err)
	}

	value, _ := dev.ReadRegU8(adc_mux_reg)

	dev.Close()

	mon_voltage := uint8(value) & mon_ax_idx

	if mon_voltage == LDO1_PG {
		result = 1.0
	}
	if mon_voltage == LDO2_PG {
		result = 0.9
	}
	if mon_voltage == LDO3_PG {
		result = 1.8
	}
	if mon_voltage == LDO4_PG {
		result = 3.3
	}
	if mon_voltage == DC1_PG {
		result = 1.325
	}
	if mon_voltage == DC2_PG {
		result = 1.1
	}
	if mon_voltage == DC3_PG {
		result = 0.9
	}

	return result
}
