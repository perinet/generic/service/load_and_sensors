package main

// #include <time.h>
import "C"

import (
	"encoding/json"
	"io"
	"net/http"
	"sync"
	"time"
)

type cpu struct {
	Id   int     `json:"id"`
	Load float64 `json:"load"`
}

type cpu_data struct {
	Cpu []cpu `json:"cpu"`
}

var (
	startTime         = time.Now()
	startTicks        = C.clock()
	cpus_mutex        sync.Mutex
	cpus              []cpu
	WaitFactor                = 1
	sample_rate       float64 = 10.0
	target_rate_mutex sync.Mutex
	target_rate       float64 = 0.0
)

func cpu_usage_percent(sample_rate float64) float64 {

	clockSeconds := float64(C.clock()-startTicks) / float64(C.CLOCKS_PER_SEC)
	realSeconds := time.Since(startTime).Seconds()

	if sample_rate > 0 && realSeconds >= sample_rate {
		startTime = time.Now()
		startTicks = C.clock()
	}
	return clockSeconds / realSeconds * 100

}

func cpu_load(id int) {
	for {
		cpus_mutex.Lock()
		cpus[id].Id = id
		cpus[id].Load = cpu_usage_percent(sample_rate)
		cpus_mutex.Unlock()
		target_rate_mutex.Lock()
		rate := target_rate
		target_rate_mutex.Unlock()
		if cpus[id].Load >= rate {
			waitTime := time.Second / time.Duration(WaitFactor)
			time.Sleep(waitTime)
		}
	}
}

func http_endpoint_load(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-type", "application/json; charset=utf-8;")
	if r.Method == http.MethodGet {
		cpus_mutex.Lock()
		var cpu cpu_data
		cpu.Cpu = cpus
		data, err := json.Marshal(cpu)
		cpus_mutex.Unlock()
		if err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			w.Write(json.RawMessage(`{"error": "cannot marshal object to json"}`))
		} else {
			w.WriteHeader(http.StatusOK)
			w.Write(data)
		}
	} else if r.Method == http.MethodPatch {

		payload, _ := io.ReadAll(r.Body)
		var load cpu
		err := json.Unmarshal(payload, &load)
		if err != nil {
			w.WriteHeader(http.StatusBadRequest)
			return
		}
		target_rate_mutex.Lock()
		target_rate = load.Load
		target_rate_mutex.Unlock()

		w.WriteHeader(http.StatusNoContent)

	} else {
		w.WriteHeader(http.StatusMethodNotAllowed)
	}
}
