PROJECT_NAME := load-and-sensor

PREFIX ?= /usr/bin

SYSTEM_DIR := /usr/lib/systemd/system

BIN_FILE := bin/$(PROJECT_NAME)
UNIT_FILE := $(PROJECT_NAME).service
README_FILE := README.md

.PHONY: all
all:service

.PHONY: service
service:
	mkdir -p bin
	go clean
	go mod tidy
	go build -o $(BIN_FILE)

.PHONY: clean
clean:
	go clean
	rm -rf bin

.PHONY: install
install: all
	# Binary
	install -d $(PREFIX)
	install -m 0755 $(BIN_FILE) $(PREFIX)

	# System unit
	install -d $(SYSTEM_DIR)
	install -m 0644 $(UNIT_FILE) $(SYSTEM_DIR)

.PHONY: uninstall
uninstall:
	rm -rf $(SYSTEM_DIR)/$(UNIT_FILE)
	rm -rf $(PREFIX)/$(PROJECT_NAME)

.PHONY: package
package: all
	tar cvzf $(PROJECT_NAME).tar.gz $(BIN_FILE) $(UNIT_FILE) $(README_FILE)
