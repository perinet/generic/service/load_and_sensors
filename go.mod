module main

go 1.19

require (
	gitlab.com/perinet/generic/apiservice/dnssd v0.0.0-20230303135503-da9c7da079e3
	gitlab.com/perinet/generic/apiservice/staticfiles v0.0.0-20221007144050-1a8dc2782f6a
	gitlab.com/perinet/generic/lib/httpserver v0.0.0-20230316155348-9361e3835c4f
	gitlab.com/perinet/generic/lib/utils v0.0.0-20221122162821-91b714770155
	gitlab.com/perinet/periMICA-container/apiservice/lifecycle v0.0.0-20221101161231-d517bb9ab0d3
	gitlab.com/perinet/periMICA-container/apiservice/node v0.0.0-20230316152325-e6e68d6ca470
	gitlab.com/perinet/periMICA-container/apiservice/security v0.0.0-20230210151025-9f2dd56cd345
)

require (
	github.com/alexandrevicenzi/go-sse v1.6.0 // indirect
	github.com/felixge/httpsnoop v1.0.1 // indirect
	github.com/godbus/dbus/v5 v5.1.0 // indirect
	github.com/google/uuid v1.3.0 // indirect
	github.com/gorilla/handlers v1.5.1 // indirect
	github.com/gorilla/mux v1.8.0 // indirect
	github.com/grantae/certinfo v0.0.0-20170412194111-59d56a35515b // indirect
	github.com/holoplot/go-avahi v1.0.1 // indirect
	golang.org/x/exp v0.0.0-20230315142452-642cacee5cc0 // indirect
)
