package main

import (
	"encoding/json"
	"flag"
	"log"
	"runtime"

	dnssd "gitlab.com/perinet/generic/apiservice/dnssd"
	static "gitlab.com/perinet/generic/apiservice/staticfiles"
	httpserver "gitlab.com/perinet/generic/lib/httpserver"
	"gitlab.com/perinet/generic/lib/httpserver/auth"
	rbac "gitlab.com/perinet/generic/lib/httpserver/rbac"
	webhelper "gitlab.com/perinet/generic/lib/utils/webhelper"
	lifecycle "gitlab.com/perinet/periMICA-container/apiservice/lifecycle"
	node "gitlab.com/perinet/periMICA-container/apiservice/node"
	security "gitlab.com/perinet/periMICA-container/apiservice/security"
)

var (
	logger         log.Logger = *log.Default()
	productionInfo node.ProductionInfo
	nodeInfo       node.NodeInfo
	securityConfig security.SecurityConfig
)

func init() {
	logger.SetPrefix("pvt: ")
	logger.Println("Starting")

	var data []byte
	// update apiservice node with used services
	services := []string{"node", "security", "lifecycle", "dns-sd"}
	data, _ = json.Marshal(services)
	webhelper.InternalPut(node.ServicesSet, data)

	var err error
	data = webhelper.InternalGet(node.NodeInfoGet)
	err = json.Unmarshal(data, &nodeInfo)
	if err != nil {
		logger.Println("Failed to fetch NodeInfo: ", err.Error())
	}

	err = json.Unmarshal(webhelper.InternalGet(node.ProductionInfoGet), &productionInfo)
	if err != nil {
		logger.Println("Failed to fetch ProductionInfo: ", err.Error())
	}

	err = json.Unmarshal(webhelper.InternalGet(security.Security_Config_Get), &securityConfig)
	if err != nil {
		logger.Println("Failed to fetch SecurityConfig: ", err.Error())
	}

	// start advertising _https via dnssd
	dnssdServiceInfo := dnssd.DNSSDServiceInfo{ServiceName: "_https._tcp", Port: 443}
	dnssdServiceInfo.TxtRecord = append(dnssdServiceInfo.TxtRecord, "api_version="+nodeInfo.ApiVersion)
	dnssdServiceInfo.TxtRecord = append(dnssdServiceInfo.TxtRecord, "life_state="+nodeInfo.LifeState)
	dnssdServiceInfo.TxtRecord = append(dnssdServiceInfo.TxtRecord, "application_name="+nodeInfo.Config.ApplicationName)
	dnssdServiceInfo.TxtRecord = append(dnssdServiceInfo.TxtRecord, "element_name="+nodeInfo.Config.ElementName)
	dnssdServiceInfo.TxtRecord = append(dnssdServiceInfo.TxtRecord, "product_name=periMICA")
	data, _ = json.Marshal(dnssdServiceInfo)
	webhelper.InternalVarsPatch(dnssd.DNSSDServiceInfoAdvertiseSet, map[string]string{"service_name": dnssdServiceInfo.ServiceName}, data)

	cpus = make([]cpu, runtime.NumCPU())
}

// Deliver API endpoints of powermeter-sdm630
func PathsGet() []httpserver.PathInfo {
	return []httpserver.PathInfo{
		{Url: "/load", Method: httpserver.GET, Role: rbac.ADMIN, Call: http_endpoint_load},
		{Url: "/load", Method: httpserver.PATCH, Role: rbac.ADMIN, Call: http_endpoint_load},
		{Url: "/pvt", Method: httpserver.GET, Role: rbac.ADMIN, Call: http_endpoint_pvt},
	}
}

func main() {
	var webui_path string
	num_processors := runtime.NumCPU()
	flag.StringVar(&webui_path, "u", "./webui", "Specify path to serve the web ui. Default is ./webui")
	flag.Parse()

	static.Set_files_path(webui_path)

	// Start obtaining sensors
	go obtain_sensors()
	// Start system load
	for i := 0; i < num_processors; i++ {
		go cpu_load(i)
	}

	if num_processors == 2 {
		define_voltages_da9063()
		define_temp_da9063()
	} else {
		define_voltages_tps65217()
		define_temp_tps65217()
	}

	go obtain_pvt(num_processors)

	// Init http server
	static.Set_files_path(webui_path)

	httpserver.AddPaths(PathsGet())
	httpserver.AddPaths(security.PathsGet())
	httpserver.AddPaths(static.PathsGet())
	httpserver.AddPaths(dnssd.PathsGet())
	httpserver.AddPaths(node.PathsGet())
	httpserver.AddPaths(lifecycle.PathsGet())

	auth.SetAuthMethod(securityConfig.ClientAuthMethod)

	httpserver.SetCertificates(httpserver.Certificates{
		CaCert:      webhelper.InternalVarsGet(security.Security_TrustAnchor_Get, map[string]string{"trust_anchor": "root_ca_cert"}),
		HostCert:    webhelper.InternalVarsGet(security.Security_Certificate_Get, map[string]string{"certificate": "host_cert"}),
		HostCertKey: webhelper.InternalVarsGet(security.Security_Certificate_Key_Get, map[string]string{"certificate": "host_cert"}),
	})

	httpserver.ListenAndServe("[::]:443")
}
