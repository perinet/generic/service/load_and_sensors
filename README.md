# System load and sensors

* Set system load
* Monitor system load
* Monitor sensors

## Installation/Execution

```shell
$make install
$systemctl start load_and_sensor
```

## system load

### Set

```shell
curl <hostname>/load -X PATCH --data '{"load": 90.0}'
```

### Get

```shell
curl <hostname>/load | jq
{
  "cpu": [
    {
      "id": 0,
      "load": 0.018959280377502233
    },
    {
      "id": 1,
      "load": 0.019725528060233068
    }
  ]
}
```

## Sensors

```shell
curl <hostname>/sensors | jq
{
  "sensors": [
    {
      "name": "f10e4078.thermal-virtual-0",
      "adapter": "Virtual device",
      "entries": [
        "temp1:        +50.8 C  "
      ]
    },
    {
      "name": "lm75-i2c-0-48",
      "adapter": "mv64xxx_i2c adapter",
      "entries": [
        "temp1:        +43.5 C  (high = +80.0 C, hyst = +75.0 C)"
      ]
    }
  ]
}
```
